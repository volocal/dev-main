<?php

namespace app\controllers;

use amnah\yii2\user\models\User;
use app\models\InformNews;
use Yii;
use app\models\News;
use app\models\newsSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('admin')) {
            $searchModel = new newsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->redirect(['/site/index']);
        }
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->id) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            return $this->redirect(['/site/index']);
        }
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can("admin")) {
            $model = new News();
            $model->author_id = 1;//Тут має бути idшка користувача
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->redirect(['/site/index']);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->can("admin") && User::findOne(['id' => Yii::$app->user->id])->role_id == 1) {
            $model = $this->findModel($id);
            $model->author_id = 1;//Тут має бути idшка користувача
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->redirect(['/site/index']);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can("admin") && User::findOne(['id' => Yii::$app->user->id])->role_id == 1) {
            $informs = InformNews::find()->where(['news_id' => $id])->all();
            foreach ($informs as $inform)
                $inform->delete();
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } else {
            return $this->redirect(['/site/index']);
        }
    }

    public function actionMassmailing($news_id)
    {
        if (Yii::$app->user->can("admin") && User::findOne(['id' => Yii::$app->user->id])->role_id == 1) {
            $news = News::findOne(['id' => $news_id]);
            if (!empty($news) && Yii::$app->user->can('admin')) {
                $users = User::find()->all();
                $admin_email = Yii::$app->params['adminEmail'];
                $messages = $this->createMessagesForNews($news);
                $subject = $news->name;
                $countEmail = $countOnline = 0;
                foreach ($users as $user) {
                    switch ($user->type_inform) {
                        case 1:
                            $this->sendNewsEmail($admin_email, $user->email, $messages, $subject);
                            $countEmail++;
                            if ($this->createInform($user->id, $news_id))
                                $countOnline++;
                            break;
                        case 2:
                            if ($this->createInform($user->id, $news_id))
                                $countOnline++;
                            break;
                        case 3:
                            $this->sendNewsEmail($admin_email, $user->email, $messages, $subject);
                            $countEmail++;
                            break;
                    }
                }
            }
            if (!empty($countOnline) || !empty($countEmail)) {
                Yii::$app->session->setFlash("Massmailing-success", 'Mails sended. Sended emails : ' . $countEmail . ' and online-mails : ' . $countOnline);
            }
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            return $this->redirect(['/site/index']);
        }
    }

    /**
     * function sends a message to the user.
     * @param $from
     * @param $to
     * @param $messages
     * @param $subject
     * @return bool
     */
    public function sendNewsEmail($from, $to, $messages, $subject)
    {
        $mailer = Yii::$app->mailer;
        $result = $mailer->compose()
            ->setFrom($from)
            ->setTo($to)
            ->setHtmlBody($messages)
            ->setSubject($subject);
        if ($result->send()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * function creates a record in the table to tell the user the news online.
     * @param $user_id
     * @param $news_id
     * @return bool
     */
    public function createInform($user_id, $news_id)
    {
        if (!InformNews::findOne(['user_id' => $user_id, 'news_id' => $news_id])) {
            $model = new InformNews;
            $model->news_id = $news_id;
            $model->user_id = $user_id;
            if ($model->save(false))
                return true;
        }
        return false;
    }

    /**
     * function creates a message body.
     * @param $news
     * @return string
     */
    public function createMessagesForNews($news)
    {
        $message = "<html><body>";
        $message .= "<p>In us news for you : " . $news->name . "</p>";
        $message .= "<p style='word-break: break-all;'>" . substr($news->text, 0, 50) . '...' . "</p>";
        $message .= "<p><a href='" . Url::to(['/news/view', 'id' => $news->id], true) . "'>Read more...</a></p>";
        $message .= "<p>Date published : " . $news->date_published . "</p>";
        $message .= "</body></html>";
        return $message;
    }

    public function clearImports()
    {
        $informs = InformNews::findAll(['user_id' => Yii::$app->user->id]);
        $old_informs = $informs;
        foreach ($informs as $inform)
            $inform->delete();
        return $old_informs;
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function beforeAction($action)
    {
        if (Yii::$app->user->id) {
            $informs = $this->clearImports();
            if ($informs != false)
                echo \Yii::$app->view->render('@app/views/site/_informs.php', ['informs' => $informs]);
        }
        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }
}
