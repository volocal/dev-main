<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `informs_news`.
 */
class m170404_190634_create_inform_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%inform_news}}', [
            'id' => Schema::TYPE_PK,
            'news_id' => Schema::TYPE_INTEGER . ' not null',
            'user_id' => Schema::TYPE_INTEGER . ' not null',
        ], $tableOptions);

        $this->addForeignKey('{{%news_news_id}}', '{{%inform_news}}', 'news_id', '{{%news}}', 'id');
        $this->addForeignKey('{{%user_user_id}}', '{{%inform_news}}', 'user_id', '{{%user}}', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('inform_news');
    }
}
