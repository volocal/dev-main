<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\newsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
$template = \amnah\yii2\user\models\User::findOne(['id' => Yii::$app->user->id])->role_id != 1 ? '{view}' : '{view}{update}{delete}';
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Yii::$app->user->can("admin")) : ?>
        <p>
            <?= Html::a('Create News', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            [
                'attribute' => 'text',
                'contentOptions' => ['style' => 'width: 40%; word-break: break-all;']
            ],
//            'text:ntext',
//            'author_id',
//            'status',
            [
                'attribute' => 'status',
                'content' => function ($data) {
                    $status = [0 => 'Не активний', 1 => 'Активний'];
                    return $status[$data->status];
                },
            ],
            // 'date_create',
            // 'date_published',
            ['class' => 'yii\grid\ActionColumn', 'template' => $template]
        ],
    ]); ?>
</div>
