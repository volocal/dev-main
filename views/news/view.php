<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use amnah\yii2\user\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">
    <?php if ($flash_success = Yii::$app->session->getFlash("Massmailing-success")) : ?>
        <div class="alert alert-success">
            <p><?= Yii::$app->session->getFlash("Massmailing-success"); ?></p>
        </div>
    <?php endif; ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (User::findOne(['id' => Yii::$app->user->id])->role_id == 1) : ?>
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Повідомити всіх про цю новину', ['massmailing', 'news_id' => $model->id], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'name',
            'text:ntext',
//            'author_id',
//            'status',
//            'date_create',
//            'date_published',
        ],
    ]) ?>

</div>
