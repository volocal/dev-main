<?php
use yii\helpers\Url;
use amnah\yii2\user\models\User;
use \yii\bootstrap\Modal;
use yii\bootstrap\Html;
use yii\web\View;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
$this->title = 'Our News';
?>
<div class="site-index">
    <form class="form-inline">
        <div class="form-group">
            <label for="pageSize">Введіть скільки новин відображати на одній сторінці(по замовчуванню 10):</label>
            <input type="number" id="pageSize" class="form-control" style="width: 70px">
            <button type="button" class="btn btn-default" onclick="redirect()">Показати</button>
        </div>
    </form>
    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
    </div>
    <div class="body-content">
        <div class="row">
            <?php foreach ($all_news as $news): ?>
                <div>
                    <h2><?= $news->name ?></h2>

                    <p style="word-break: break-all;"><?= substr($news->text, 0, 50) . '...' ?></p>
                    <?php if (!Yii::$app->user->isGuest) : ?>
                        <p><a class="btn btn-default" href="<?= Url::toRoute(['/news/view', 'id' => $news->id]) ?>">Read
                                more...</a></p>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
            <?= LinkPager::widget([
                'pagination' => $pages,
            ]); ?>
        </div>

    </div>
</div>
<?php
$url = Url::toRoute('site/index',true);
$script = <<< JS
    function redirect() {
    pageSize = $('#pageSize').val();
    baseUrl = "$url";
    window.location = baseUrl + "?pageSize=" + pageSize;
    }
JS;
$this->registerJs($script, View::POS_BEGIN);
?>
