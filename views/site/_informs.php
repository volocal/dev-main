<?php
use \yii\bootstrap\Modal;
use \app\models\News;
use yii\helpers\Url;

?>
<?php
Modal::begin([
    'header' => '<h4>Count News : ' . count($informs) . ' </h4>',
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, 'show' => 'true']
]);
echo "<div id='modalContent'>";
foreach ($informs as $inform) {
    $news = News::findOne(['id' => $inform->news_id]);
    if (!empty($news)) {
        echo "<div id='news_" . $news->id . "'>";
        echo "<h2>" . $news->name . "</h2>";
        echo "<h3>" . substr($news->text, 0, 50) . '...' . "</h3>";
        echo "<p><a href='" . Url::to(['/news/view', 'id' => $news->id], true) . "'>Read more...</a></p>";
        echo "</div>";
    }
}
echo "</div>";
Modal::end();
?>
