<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=nameYourBD',
    'username' => 'yourUserName',
    'password' => 'yourPassword',
    'charset' => 'utf8',
];
