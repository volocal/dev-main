<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property integer $author_id
 * @property integer $status
 * @property string $date_create
 * @property string $date_published
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text', 'author_id'], 'required'],
            [['text'], 'string'],
            [['author_id', 'status'], 'integer'],
            [['date_create', 'date_published'], 'safe'],
            [['name'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'text' => 'Text',
            'author_id' => 'Author ID',
            'status' => 'Status',
            'date_create' => 'Date Create',
            'date_published' => 'Date Published',
        ];
    }
}
